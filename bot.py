# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 08:30:11 2019

@author: Vito
"""

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import time
import os

firefox_profile = webdriver.FirefoxProfile()
firefox_profile.set_preference("browser.download.folderList",2)
firefox_profile.set_preference("browser.download.manager.showWhenStarting",False)
firefox_profile.set_preference("browser.download.dir", os.getcwd()+'\\dataset\\downloadBot\\')
firefox_profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream")

binary = FirefoxBinary('C:/Program Files/Mozilla Firefox/firefox.exe')
path = "C:/Program Files/GeckoDriver/geckodriver.exe"
driver = webdriver.Firefox(firefox_binary = binary, executable_path = path, firefox_profile = firefox_profile)


def baixaDespesas(driver):
    driver.get('https://transparencia.es.gov.br/DadosAbertos/BaseDeDados')
    
    despesas = driver.find_element_by_xpath('/html/body/div[4]/div/div[3]/div/div[11]/div[1]')
    despesas.click()
    
    i = '1'
    indice = 1
    while(True):        
        xpathAno = ('/html/body/div[4]/div/div[3]/div/div[11]/div[2]/div/div/table/tbody/tr[2]/td[4]/select/option['+i+']')
        try:
            anos = driver.find_element_by_xpath(xpathAno)
        except: 
            print('Fim da lista de downloads')
            break
        anos.click()
        
        indice += 1
        i = str(indice)
    
        baixar = driver.find_element_by_xpath('/html/body/div[4]/div/div[3]/div/div[11]/div[2]/div/div/table/tbody/tr[2]/td[5]/a')
        baixar.click()
        time.sleep(15)
        
        
    
baixaDespesas(driver)

#driver.close()


