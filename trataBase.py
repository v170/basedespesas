# -*- coding: utf-8 -*-

"""
Created on Fri Mar 29 17:57:58 2019

@author: Vito
"""
import pandas as pd

# IMPORTAÇÃO DA BASE DE DADOS, CONFIGURANDO O SEPARADOR COMO ";" :
base = pd.read_csv('dataset/Despesas-2019.csv', sep=';')

# CONVERSÃO DAS DATAS DO FORMATO "dd/mm/aaaa" PARA "yyyy-MM-dd":
def trataData(base):
    baseData = base.copy()
    for i in baseData.index:
        data = (baseData.at[i,"Data"]).split(' ')
        data[0] = data[0].split('/')
        data[0] = [data[0][2], data[0][1], data[0][0]]
        data[0] = '-'.join(data[0])
        data = ' '.join(data)
        baseData.at[i, "Data"] = data
    return baseData

# CONVERSÃO DAS LETRAS MAIÚSCULAS PARA LETRAS MINÚSCULAS POR TODA A BASE:
def trataMaiuscula(baseMaiuscula):
    return baseMaiuscula.applymap(lambda celula:celula.lower() if type(celula)==str else celula)


# RETIRADA DE TODOS ACENTOS E SINAIS DE CEDILHA:
def trataAcento(baseAcentuada):
    return baseAcentuada.applymap(lambda celula:executaRetiraAcento(celula) if type(celula) == str else celula)

def executaRetiraAcento(base):
    letras = list()
    for l in base:
        letras.append(retiraAcento(l))
    return "".join(letras)

def retiraAcento(letra):
    if letra == "\n": return letra.replace("\n", " ")
    letraByte = str.encode(letra)
    if letraByte < b'\xc3\x80': return letra
    if letraByte <= b'\xc3\x85': return 'A'
    if letraByte == b'\xc3\x87': return 'C'
    if letraByte >= b'\xc3\x88' and letraByte <= b'\xc3\x8b':
        return 'E'
    if letraByte >= b'\xc3\x8c' and letraByte <= b'\xc3\x8f': 
        return 'I'
    if letraByte >= b'\xc3\x92' and letraByte <= b'\xc3\x96':
        return 'O'
    if letraByte >= b'\xc3\x99' and letraByte <= b'\xc3\x9c':
        return 'U'
    if letraByte >= b'\xc3\xa0' and letraByte <= b'\xc3\xa5':
        return 'a'
    if letraByte == b'\xc3\xa7': return 'c'
    if letraByte >= b'\xc3\xa8' and letraByte <= b'\xc3\xab':
        return 'e'
    if letraByte >= b'\xc3\xac' and letraByte <= b'\xc3\xaf':
        return 'i'
    if letraByte >= b'\xc3\xb2' and letraByte <= b'\xc3\xb6':
        return 'o'
    if letraByte >= b'\xc3\xb9' and letraByte <= b'\xc3\xbc':
        return 'u'   
    else: return letra

# EXECUÇÃO DE CADA FUNÇÃO INDIVIDUAL APENAS PARA TESTE
#baseMinuscula = trataMaiuscula(base)    
#baseData = trataData(base)
#baseAcento = trataAcento(base)

# EXECUÇÃO DAS FUNÇÕES EM CIMA DA BASE. TEM COMO SAÍDA A DATAFRAME "baseTratada"
baseTratada = trataMaiuscula(base)
baseTratada = trataData(baseTratada)
baseTratada = trataAcento(baseTratada)

baseTratada.to_csv('basesTratadas/csv/baseTratada.csv', sep=';', index=False, line_terminator='\n')

#baseTratadaTesteCsv = pd.read_csv('basesTratadas/csv/baseTratada.csv', sep=';', lineterminator='\n')

baseTratada.to_parquet('basesTratadas/parquet/baseTratada.parquet.gzip', compression='gzip')

#baseTratadaTesteParquet = pd.read_parquet('basesTratadas/parquet/baseTratada.parquet.gzip')





























